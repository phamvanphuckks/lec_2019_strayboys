int x; 
#define BAUD (9600)


void setup() 
{
  Serial.begin(BAUD);
  pinMode(6,OUTPUT); // Enable pin 
  pinMode(5,OUTPUT); // Step pin
  pinMode(4,OUTPUT); // Dir - pin
  digitalWrite(6,LOW); // Set Enable low
}

void loop() 
{
  digitalWrite(6,LOW); 
  digitalWrite(4,HIGH); 
  Serial.println("Cho chay 200 steps (1 vong)");
  for(x = 0; x < 200; x++) // Cho chay 1 vong
  {
    digitalWrite(5,HIGH); // Output high
    delay(10); 
    digitalWrite(5,LOW); // Output low
    delay(100); 
  }
  Serial.println("Pause");
  delay(1000); 
}
