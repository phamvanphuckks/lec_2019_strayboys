#define trig 2 
#define echo 3 
#define relay 7 
void setup() {
e:
  Serial.begin(9600);
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  pinMode(relay, OUTPUT);
}

void loop() {
  
  unsigned long duration; 
  int distance; 

  /*phat xung tu chan trig*/
  digitalWrite(trig, 0); 
  delayMicroseconds(2);
  digitalWrite(trig,1); 
  delayMicroseconds(5);
  digitalWrite(trig,0);

  /*tinh toan thoi gian */
  //do do rong xung HIGH o chan ECHO
  duration = pulseIn(echo, HIGH);
  //tinh khoang cach den vat
  distance = int (duration/2/29.412);
  Serial.println(distance);


  if(distance >=4 )
  {
    digitalWrite(relay, HIGH);
    Serial.println("On");
  }
  if(distance < 4 && distance > 2)
  {
    digitalWrite(relay, LOW);
    Serial.println("Off");
  }
  
  
}
