var udoo = require('udoo-gpio') //Import the module

	var led = 2 //declare led: GPIO pin 40 --> Arduino pin 13

	//Setup our pin
	function setup() {
	    udoo.pinMode(led, "OUTPUT")
	    udoo.digitalWrite(led, "HIGH") //make sure the led is off
	}

	//set a variable off that triggers when the delay is done
	var off = function(){
		udoo.digitalWrite(led, "HIGH")
	}

	setup() //call function setup
	udoo.digitalWrite(led, "HIGH")//turn on our led
	udoo.delay(2, off) //wait 3 seconds and turn off
