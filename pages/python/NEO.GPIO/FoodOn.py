from neo import Gpio
from time import sleep

neo = Gpio()
writepin = 8
    
neo.pinMode(writepin, neo.OUTPUT)
    
while True:
  neo.digitalWrite(writepin, neo.HIGH)
  sleep(1)
  print "Pin 23 high current state is: "+str(neo.digitalRead(writepin))
  neo.digitalWrite(writepin, neo.LOW)
  sleep(1)
  print "Pin 23 low current state is: "+str(neo.digitalRead(writepin))
